/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 11:43:01 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/10 19:11:36 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/md5.h"

/*
** handle long md5 input
** process every 64 char of input string
*/

int				ft_md5_long(char *input, t_md5 *digest, t_context *context)
{
	t_word		*tmp_offset;
	char		*tmp_char[4];
	t_word		tmp_int[4];
	int			input_len;
	int			i;
	int			j;

	tmp_offset = (t_word *)malloc(digest->offset_len * sizeof(t_word));
	ft_bzero(tmp_offset, digest->offset_len * sizeof(t_word));
	input_len = ft_strlen(input);
	i = 64;
	while (i <= input_len && input_len >= 64)
	{
		input = ft_strsub(input, i - 64, i + (i - input_len % 64));
		j = 0;
		ft_putendl(input);
		while (j < 64 && input && input[j + 3])
		{
			tmp_char[0] = ft_strsub(input, j, 1);
			tmp_int[0] = (t_word)*tmp_char[0];
			tmp_char[1] = ft_strsub(input, j + 1, 1);
			tmp_int[1] = (t_word)*tmp_char[1] << 8;
			tmp_char[2] = ft_strsub(input, j + 2, 1);
			tmp_int[2] = (t_word)*tmp_char[2] << 16;
			tmp_char[3] = ft_strsub(input, j + 3, 1);
			tmp_int[3] = (t_word)*tmp_char[3] << 24;
			tmp_offset[j >> 2] = tmp_int[0] + tmp_int[1] + tmp_int[2]\
														 + tmp_int[3]; 
			free(tmp_char[0]);
			free(tmp_char[1]);
			free(tmp_char[2]);
			free(tmp_char[3]);
			j += 4;
		}
		*context = ft_blocks_operations(context, tmp_offset);
		i += 64;
	}
	free(tmp_offset);
	return (i);
}
