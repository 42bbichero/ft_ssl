/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_blocks_operations.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/28 10:44:37 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/08 20:30:37 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/md5.h"

/*
** Process offset
*/

t_context			ft_blocks_operations(t_context *context, t_word *offset)
{
	t_word			j;
	t_word			x[16];
	t_context		back_context;

	j = -1;
	ft_bzero(x, 16);
	back_context = *context;
	while (++j < 16)
		x[j] = offset[j];
	ft_round_1_1(context, x);
	ft_round_1_2(context, x);
	ft_round_2_1(context, x);
	ft_round_2_2(context, x);
	ft_round_3_1(context, x);
	ft_round_3_2(context, x);
	ft_round_4_1(context, x);
	ft_round_4_2(context, x);
	back_context.a += context->a;
	back_context.b += context->b;
	back_context.c += context->c;
	back_context.d += context->d;
	return (back_context);
}

/*
** First operations round
** context need to be initialize before each call to main function
** because word arguments are not in the same order
*/

void				ft_round_1_1(t_context *context, t_word *x)
{
	context->a = ft_main_f(*context, x[0], S11, 0);
	context->d = ft_main_f(ft_init_context(context->d, context->a, context->b,\
													context->c), x[1], S12, 1);
	context->c = ft_main_f(ft_init_context(context->c, context->d, context->a,\
													context->b), x[2], S13, 2);
	context->b = ft_main_f(ft_init_context(context->b, context->c, context->d,\
													context->a), x[3], S14, 3);
	context->a = ft_main_f(ft_init_context(context->a, context->b, context->c,\
													context->d), x[4], S11, 4);
	context->d = ft_main_f(ft_init_context(context->d, context->a, context->b,\
													context->c), x[5], S12, 5);
	context->c = ft_main_f(ft_init_context(context->c, context->d, context->a,\
													context->b), x[6], S13, 6);
	context->b = ft_main_f(ft_init_context(context->b, context->c, context->d,\
													context->a), x[7], S14, 7);
}

/*
** First operations round (second part)
** context need to be initialize before each call to main function
** because word arguments are not in the same order
*/

void				ft_round_1_2(t_context *context, t_word *x)
{
	context->a = ft_main_f(ft_init_context(context->a, context->b, context->c,\
												context->d), x[8], S11, 8);
	context->d = ft_main_f(ft_init_context(context->d, context->a, context->b,\
												context->c), x[9], S12, 9);
	context->c = ft_main_f(ft_init_context(context->c, context->d, context->a,\
												context->b), x[10], S13, 10);
	context->b = ft_main_f(ft_init_context(context->b, context->c, context->d,\
												context->a), x[11], S14, 11);
	context->a = ft_main_f(ft_init_context(context->a, context->b, context->c,\
												context->d), x[12], S11, 12);
	context->d = ft_main_f(ft_init_context(context->d, context->a, context->b,\
												context->c), x[13], S12, 13);
	context->c = ft_main_f(ft_init_context(context->c, context->d, context->a,\
												context->b), x[14], S13, 14);
	context->b = ft_main_f(ft_init_context(context->b, context->c, context->d,\
												context->a), x[15], S14, 15);
}

/*
** Second operations round
** context need to be initialize before each call to main function
** because word arguments are not in the same order
*/

void				ft_round_2_1(t_context *context, t_word *x)
{
	context->a = ft_main_g(ft_init_context(context->a, context->b, context->c,\
												context->d), x[1], S21, 16);
	context->d = ft_main_g(ft_init_context(context->d, context->a, context->b,\
												context->c), x[6], S22, 17);
	context->c = ft_main_g(ft_init_context(context->c, context->d, context->a,\
												context->b), x[11], S23, 18);
	context->b = ft_main_g(ft_init_context(context->b, context->c, context->d,\
												context->a), x[0], S24, 19);
	context->a = ft_main_g(ft_init_context(context->a, context->b, context->c,\
												context->d), x[5], S21, 20);
	context->d = ft_main_g(ft_init_context(context->d, context->a, context->b,\
												context->c), x[10], S22, 21);
	context->c = ft_main_g(ft_init_context(context->c, context->d, context->a,\
												context->b), x[15], S23, 22);
	context->b = ft_main_g(ft_init_context(context->b, context->c, context->d,\
												context->a), x[4], S24, 23);
}

/*
** Second operations round (second part)
** context need to be initialize before each call to main function
** because word arguments are not in the same order
*/

void				ft_round_2_2(t_context *context, t_word *x)
{
	context->a = ft_main_g(ft_init_context(context->a, context->b, context->c,\
												context->d), x[9], S21, 24);
	context->d = ft_main_g(ft_init_context(context->d, context->a, context->b,\
												context->c), x[14], S22, 25);
	context->c = ft_main_g(ft_init_context(context->c, context->d, context->a,\
												context->b), x[3], S23, 26);
	context->b = ft_main_g(ft_init_context(context->b, context->c, context->d,\
												context->a), x[8], S24, 27);
	context->a = ft_main_g(ft_init_context(context->a, context->b, context->c,\
												context->d), x[13], S21, 28);
	context->d = ft_main_g(ft_init_context(context->d, context->a, context->b,\
												context->c), x[2], S22, 29);
	context->c = ft_main_g(ft_init_context(context->c, context->d, context->a,\
												context->b), x[7], S23, 30);
	context->b = ft_main_g(ft_init_context(context->b, context->c, context->d,\
												context->a), x[12], S24, 31);
}
