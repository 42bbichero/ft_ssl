/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fct_process_messages.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 16:14:59 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/08 18:50:48 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/md5.h"

/*
** F() first operations round function
*/

t_word			ft_main_f(t_context context, t_word x, t_word padding_l_len,\
																	t_word i)
{
	t_word		res;

	res = ft_aux_f(context.b, context.c, context.d);
	res = (context.a + res) + (x + md5_table[i]);
	res = ft_rot_to_l(res, padding_l_len) + context.b;
	return (res);
}

/*
** G() second operations round function
*/

t_word			ft_main_g(t_context context, t_word x, t_word padding_l_len,\
																	t_word i)
{
	t_word		res;

	res = ft_aux_g(context.b, context.c, context.d);
	res = (context.a + res) + (x + md5_table[i]);
	res = ft_rot_to_l(res, padding_l_len) + context.b;
	return (res);
}

/*
** H() third operations round function
*/

t_word			ft_main_h(t_context context, t_word x, t_word padding_l_len,\
																	t_word i)
{
	t_word		res;

	res = ft_aux_h(context.b, context.c, context.d);
	res = (context.a + res) + (x + md5_table[i]);
	res = ft_rot_to_l(res, padding_l_len) + context.b;
	return (res);
}

/*
** I() fourth operations round function
*/

t_word			ft_main_i(t_context context, t_word x, t_word padding_l_len,\
																	t_word i)
{
	t_word		res;

	res = ft_aux_i(context.b, context.c, context.d);
	res = (context.a + res) + (x + md5_table[i]);
	res = ft_rot_to_l(res, padding_l_len) + context.b;
	return (res);
}
