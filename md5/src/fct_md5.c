/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 11:43:01 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/10 19:11:26 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/md5.h"

/*
**	Create new context with wanted values
*/

t_context		ft_init_context(t_word a, t_word b, t_word c, t_word d)
{
	t_context	context;

	context.a = a;
	context.b = b;
	context.c = c;
	context.d = d;
	return (context);
}

/*
** Add message len to last 64 bits of offset
*/

void			ft_add_len_message_len(t_md5 *digest)
{
	int			i;

	i = digest->offset_len - digest->len_message_len;
	digest->offset[14] = digest->len_message_len;
}

/*
** Fill all metadata of digest t_md5 struct
** Malloc offset to stock all words
** digest->len is in bytes and we need a bit value
** Malloc must be with a multiple of bytes and not sizeof(char)
** 
*/

t_md5			*ft_digest_metadata(char *input, t_context *context)
{
	t_md5		*digest;
	int			i;
	int			input_len;

	digest = (t_md5 *)malloc(sizeof(t_md5));
	digest->message_len = (t_word)ft_strlen(input);
	digest->len_message_len = 8 * (sizeof(char) * digest->message_len);
	digest->offset_len = 16;
	i = ft_md5_long(input, digest, context);
	input = ft_strsub(input, i - 64, ft_strlen(input) - (i - 64));
	//ft_putendl(ft_itoa_base(ft_swap(context->a), 16));
	digest->input = ft_strdup(input);
	free(input);
	input_len = ft_strlen(digest->input);
	digest->offset = (t_word *)malloc(digest->offset_len * sizeof(t_word));
	ft_bzero(digest->offset, digest->offset_len * sizeof(t_word) + 1);
	i = -1;
	while (++i < input_len)
		digest->offset[i >> 2] |= digest->input[i] << ((i % 4) << 3);
	digest->offset[i >> 2] |= 0x80 << ((i % 4) << 3);
	return (digest);
}

/*
** Do some checks then execute blocks operations
*/

t_context		*ft_cycles(t_md5 *digest, t_context *context)
{
	t_word		input_len;

	input_len = ft_strlen(digest->input);
	if (ft_strlen(digest->input) > 55)
	{
		*context = ft_blocks_operations(context, digest->offset);
		ft_bzero(digest->offset, digest->offset_len * sizeof(t_word) + 1);
	}
	ft_add_len_message_len(digest);
	*context = ft_blocks_operations(context, digest->offset);
	return (context);
}

/*
** main function for MD5 algorithm
** Return result hash
*/

char			*ft_md5(char *input)
{
	t_md5		*digest;
	t_context	context;
	char		*md5_hash;

	context = ft_init_context(A, B, C, D);
	digest = ft_digest_metadata(input, &context);
	ft_cycles(digest, &context);
	md5_hash = ft_context_to_result(context);
	free(digest->input);
	free(digest->offset);
	free(digest);
	return (md5_hash);
}
