/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bitwise_operations.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 11:48:47 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/05 16:32:19 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/md5.h"

t_word			ft_rot_to_r(t_word n, t_word rot_size)
{
	return ((n << rot_size | n << (WORD_SIZE - rot_size)));
}

t_word			ft_rot_to_l(t_word n, t_word rot_size)
{
	return ((n << rot_size | n >> (WORD_SIZE - rot_size)));
}
