/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fct_process_messages_aux.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 16:15:10 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/05 16:15:25 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/md5.h"

t_word			ft_aux_f(t_word x, t_word y, t_word z)
{
	return ((x & y) | (~x & z));
}

t_word			ft_aux_g(t_word x, t_word y, t_word z)
{
	return ((x & z) | (y & ~z));
}

t_word			ft_aux_h(t_word x, t_word y, t_word z)
{
	return (x ^ y ^ z);
}

t_word			ft_aux_i(t_word x, t_word y, t_word z)
{
	return (y ^ (x | ~z));
}
