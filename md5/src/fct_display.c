/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fct_display.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 20:37:38 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/09 14:39:31 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/md5.h"

/*
** Swap 32 bit integer
** 0xabcdefgh => 0xghefbcab
** move byte 3 to byte 0
** move byte 1 to byte 2
** move byte 2 to byte 1
** byte 0 to byte 3
*/

t_word			ft_swap(int addr)
{
	t_word		new;

	new = ((addr >> 24) & 0xff) |
			((addr << 8) & 0xff0000) |
			((addr >> 8) & 0xff00) |
			((addr << 24) & 0xff000000);
	return (new);
}

/*
** result final str result of md5 algorithm
** Hexadecimal value need to be in lower case
*/

char			*ft_context_to_result(t_context context)
{
	char		*word;
	char		*md5_hash;

	md5_hash = ft_strnew(32);
	word = ft_itoa_base(ft_swap(context.a), 16);	
	ft_strncpy(md5_hash, word, 8);
	free(word);
	word = ft_itoa_base(ft_swap(context.b), 16);
	ft_strncpy(&md5_hash[8], word, 8);
	free(word);
	word = ft_itoa_base(ft_swap(context.c), 16);
	ft_strncpy(&md5_hash[16], word, 8);
	free(word);
	word = ft_itoa_base(ft_swap(context.d), 16);
	ft_strncpy(&md5_hash[24], word, 8);
	free(word);
	word = ft_strtolower(md5_hash);
	free(md5_hash);
	return (word);
}
