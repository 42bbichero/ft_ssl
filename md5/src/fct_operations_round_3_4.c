/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fct_operations_round_3_4.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/01 13:56:47 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/05 16:19:58 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/md5.h"

/*
** third operations round
** context need to be initialize before each call to main function
** because word arguments are not in the same order
*/

void				ft_round_3_1(t_context *context, t_word *x)
{
	context->a = ft_main_h(ft_init_context(context->a, context->b, context->c,\
												context->d), x[5], S31, 32);
	context->d = ft_main_h(ft_init_context(context->d, context->a, context->b,\
												context->c), x[8], S32, 33);
	context->c = ft_main_h(ft_init_context(context->c, context->d, context->a,\
												context->b), x[11], S33, 34);
	context->b = ft_main_h(ft_init_context(context->b, context->c, context->d,\
												context->a), x[14], S34, 35);
	context->a = ft_main_h(ft_init_context(context->a, context->b, context->c,\
												context->d), x[1], S31, 36);
	context->d = ft_main_h(ft_init_context(context->d, context->a, context->b,\
												context->c), x[4], S32, 37);
	context->c = ft_main_h(ft_init_context(context->c, context->d, context->a,\
												context->b), x[7], S33, 38);
	context->b = ft_main_h(ft_init_context(context->b, context->c, context->d,\
												context->a), x[10], S34, 39);
}

/*
** third operations round (second part)
** context need to be initialize before each call to main function
** because word arguments are not in the same order
*/

void				ft_round_3_2(t_context *context, t_word *x)
{
	context->a = ft_main_h(ft_init_context(context->a, context->b, context->c,\
												context->d), x[13], S31, 40);
	context->d = ft_main_h(ft_init_context(context->d, context->a, context->b,\
												context->c), x[0], S32, 41);
	context->c = ft_main_h(ft_init_context(context->c, context->d, context->a,\
												context->b), x[3], S33, 42);
	context->b = ft_main_h(ft_init_context(context->b, context->c, context->d,\
												context->a), x[6], S34, 43);
	context->a = ft_main_h(ft_init_context(context->a, context->b, context->c,\
												context->d), x[9], S31, 44);
	context->d = ft_main_h(ft_init_context(context->d, context->a, context->b,\
												context->c), x[12], S32, 45);
	context->c = ft_main_h(ft_init_context(context->c, context->d, context->a,\
												context->b), x[15], S33, 46);
	context->b = ft_main_h(ft_init_context(context->b, context->c, context->d,\
												context->a), x[2], S34, 47);
}

/*
** fourth operations round
** context need to be initialize before each call to main function
** because word arguments are not in the same order
*/

void				ft_round_4_1(t_context *context, t_word *x)
{
	context->a = ft_main_i(ft_init_context(context->a, context->b, context->c,\
												context->d), x[0], S41, 48);
	context->d = ft_main_i(ft_init_context(context->d, context->a, context->b,\
												context->c), x[7], S42, 49);
	context->c = ft_main_i(ft_init_context(context->c, context->d, context->a,\
												context->b), x[14], S43, 50);
	context->b = ft_main_i(ft_init_context(context->b, context->c, context->d,\
												context->a), x[5], S44, 51);
	context->a = ft_main_i(ft_init_context(context->a, context->b, context->c,\
												context->d), x[12], S41, 52);
	context->d = ft_main_i(ft_init_context(context->d, context->a, context->b,\
												context->c), x[3], S42, 53);
	context->c = ft_main_i(ft_init_context(context->c, context->d, context->a,\
												context->b), x[10], S43, 54);
	context->b = ft_main_i(ft_init_context(context->b, context->c, context->d,\
												context->a), x[1], S44, 55);
}

/*
** Fourth operations round (second part)
** context need to be initialize before each call to main function
** because word arguments are not in the same order
*/

void				ft_round_4_2(t_context *context, t_word *x)
{
	context->a = ft_main_i(ft_init_context(context->a, context->b, context->c,\
												context->d), x[8], S41, 56);
	context->d = ft_main_i(ft_init_context(context->d, context->a, context->b,\
												context->c), x[15], S42, 57);
	context->c = ft_main_i(ft_init_context(context->c, context->d, context->a,\
												context->b), x[6], S43, 58);
	context->b = ft_main_i(ft_init_context(context->b, context->c, context->d,\
												context->a), x[13], S44, 59);
	context->a = ft_main_i(ft_init_context(context->a, context->b, context->c,\
												context->d), x[4], S41, 60);
	context->d = ft_main_i(ft_init_context(context->d, context->a, context->b,\
												context->c), x[11], S42, 61);
	context->c = ft_main_i(ft_init_context(context->c, context->d, context->a,\
												context->b), x[2], S43, 62);
	context->b = ft_main_i(ft_init_context(context->b, context->c, context->d,\
												context->a), x[9], S44, 63);
}
