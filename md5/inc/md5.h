/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 11:40:45 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/10 13:43:20 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MD5_H
# define MD5_H
# include "../../inc/ft_ssl.h"

/*
** Define 4 constants context word value
*/

# define A 0x67452301
# define B 0xefcdab89
# define C 0x98badcfe
# define D 0x10325476

/*
** Define constant padding left value for context calcul
** But don't know how this value are choosing
*/

# define S11 7
# define S12 12
# define S13 17
# define S14 22
# define S21 5
# define S22 9
# define S23 14
# define S24 20
# define S31 4
# define S32 11
# define S33 16
# define S34 23
# define S41 6
# define S42 10
# define S43 15
# define S44 21

static const t_word md5_table[64] = {
	0xd76aa478, 0xE8C7B756, 0x242070DB, 0xC1BDCEEE,
	0xF57C0FAF, 0x4787C62A, 0xA8304613, 0xFD469501,
	0x698098D8, 0x8B44F7AF, 0xFFFF5BB1, 0x895CD7BE,
	0x6B901122, 0xFD987193, 0xA679438E, 0x49B40821,
	0xF61E2562, 0xC040B340, 0x265E5A51, 0xE9B6C7AA,
	0xD62F105D, 0x02441453, 0xD8A1E681, 0xE7D3FBC8,
	0x21E1CDE6, 0xC33707D6, 0xF4D50D87, 0x455A14ED,
	0xA9E3E905, 0xFCEFA3F8, 0x676F02D9, 0x8D2A4C8A,
	0xFFFA3942, 0x8771F681, 0x6d9d6122, 0xFDE5380C,
	0xA4BEEA44, 0x4BDECFA9, 0xF6BB4B60, 0xBEBFBC70,
	0x289B7EC6, 0xEAA127FA, 0xD4EF3085, 0x04881D05,
	0xD9D4D039, 0xE6DB99E5, 0x1FA27CF8, 0xC4AC5665,
	0xF4292244, 0x432AFF97, 0xAB9423A7, 0xFC93A039,
	0x655B59C3, 0x8F0CCC92, 0xFFEFF47D, 0x85845DD1,
	0x6FA87E4F, 0xFE2CE6E0, 0xA3014314, 0x4E0811A1,
	0xF7537E82, 0xBD3AF235, 0x2AD7D2BB, 0xEB86D391 };

/*
** All values are in bytes NOT IN bits
** len 				-> size in bytes of array of words padded
** padding_len 		-> res of % operation
** message_len 		-> size in bytes of input message
** len_message_len  -> size in bytes of input message len
** x[16]			-> tmp word tab used for rounding
** offset str		-> contain offset for message and message len
*/

typedef struct			s_md5
{
	t_word				offset_len;
	t_word				padding_len;
	t_word				message_len;
	t_word				len_message_len;
	t_word				*offset;
	char				*input;
}						t_md5;

typedef struct			s_context
{
	uint64_t			a;
	uint64_t			b;
	uint64_t			c;
	uint64_t			d;
}						t_context;

void					ft_wiki_block(char *input);

/*
** Bits operations fuctions
*/

t_word					ft_rot_to_r(t_word c, t_word rot_size);
t_word					ft_rot_to_l(t_word c, t_word rot_size);

/*
** Functions used for process messages in 16 word blocks
*/

t_word					ft_aux_f(t_word x, t_word y, t_word z);
t_word					ft_aux_g(t_word x, t_word y, t_word z);
t_word					ft_aux_h(t_word x, t_word y, t_word z);
t_word					ft_aux_i(t_word x, t_word y, t_word z);

/*
** Main process message functions for round
*/

t_word					ft_main_f(t_context context, t_word x, t_word i,\
													t_word padding_l_len);
t_word					ft_main_g(t_context context, t_word x, t_word i,\
													t_word padding_l_len);
t_word					ft_main_h(t_context context, t_word x, t_word i,\
													t_word padding_l_len);
t_word					ft_main_i(t_context context, t_word x, t_word i,\
													t_word padding_l_len);

/*
** Padding functions
*/

t_word					ft_padding_zero(t_md5 *digest, char *input);
void					ft_add_len_message_len(t_md5 *digest);
t_md5					*ft_digest_metadata(char *input, t_context *context);

/*
** Functin used for general display
*/

t_word					ft_swap(int addr);
char					*ft_context_to_result(t_context context);

/*
** Initialisation fuctions
*/

t_context				ft_init_context(t_word a, t_word b, t_word c,\
															t_word d);

/*
** Blocks operations functions
** Main function ft_blocks_operations, operate 4 round
** for each 16 blocks offset
*/

t_context				*ft_cycles(t_md5 *digest,\
												t_context *context);
int						ft_md5_long(char *input, t_md5 *digest,\
												t_context *context);
t_context				ft_blocks_operations(t_context *context,
												t_word *offset);

/*
** Operation round functions
*/

void					ft_round_1_1(t_context *context, t_word *x);
void					ft_round_1_2(t_context *context, t_word *x);
void					ft_round_2_1(t_context *context, t_word *x);
void					ft_round_2_2(t_context *context, t_word *x);
void					ft_round_3_1(t_context *context, t_word *x);
void					ft_round_3_2(t_context *context, t_word *x);
void					ft_round_4_1(t_context *context, t_word *x);
void					ft_round_4_2(t_context *context, t_word *x);

#endif
