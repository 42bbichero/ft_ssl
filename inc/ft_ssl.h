/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 11:40:45 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/09 15:22:48 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL
# define BINARY_SIZE 32

# if defined __WINT_WIDTH__
	# define WORD_SIZE __WINT_WIDTH__
# else
	# define WORD_SIZE 32
# endif

# define DEBUG 0

# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# include "../../libft/libft.h"

# if __WINT_WIDTH__ == 32
	typedef uint32_t		t_word;
# elif __WINT_WIDTH__ == 16
	typedef uint16_t		t_word;
# endif

/*
** Define hash algorithm informations
*/

typedef struct			s_hash {
	char				*hash_name;
	char				*(*fun)(char *);
}						t_hash;

/*
** Define hash functions
*/

char					*ft_md5(char *input);

/*
** Hash function pointer array
** easy to add another algorithm
*/

static const t_hash		fct_hash[2] = {
 { "ft_md5", &ft_md5 },
 { 0, 0 }
};

#endif
