ft_ssl
------

### Documentation: 
[Secure Hash Standard](https://csrc.nist.gov/csrc/media/publications/fips/180/4/final/documents/fips180-4-draft-aug2014.pdf)   
[MD5 digest doc](https://tools.ietf.org/html/rfc1321.html)   
[SHAs algo](https://tools.ietf.org/html/rfc6234)   
[SHAs digest doc](http://www.iwar.org.uk/comsec/resources/cipher/sha256-384-512.pdf)   
[Bitwise operation](http://graphics.stanford.edu/~seander/bithacks.html#BitReverseObvious)
