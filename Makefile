# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bbichero <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/02/27 11:35:09 by bbichero          #+#    #+#              #
#    Updated: 2019/03/10 17:29:10 by bbichero         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ssl
TEST_NAME = run_test

# colors
Y = \033[0;33m
R = \033[0;31m
G = \033[0;32m
E = \033[39m

# Compiler
CC = gcc

# Source files
MAIN = main.c
SRC_MD5 = fct_bitwise_operations.c fct_process_messages.c \
	  fct_process_messages_aux.c fct_display.c fct_md5.c \
	  fct_block_operations.c fct_operations_round_3_4.c fct_cycles.c

TEST = test.c

SRCS_MD5 = $(addprefix ./$(SRC_MD5_DIR)/, $(SRC_MD5))

# Directories
MD5_DIR = md5
SRC_MD5_DIR = $(MD5_DIR)/src
INC_MD5_DIR = $(MD5_DIR)/inc
OBJ_MD5_DIR = $(MD5_DIR)/obj
TEST_DIR = tests
LIBFT_DIR = ../libft

# Include
GLOBAL_H = inc/ft_ssl.h
MD5_H = $(INC_MD5_INC)/md5.h

# Others commands
RM = /bin/rm

# binaries
OBJ_MD5 = $(SRC_MD5:.c=.o)
OBJ_MAIN = $(MAIN:.c=.o)
OBJ_TEST = $(TEST:.c=.o)

OBJS_MD5 = $(addprefix ./$(OBJ_MD5_DIR)/, $(OBJ_MD5))
OBJS_TEST = $(addprefix ./$(TEST_DIR)/, $(OBJ_TEST))
LIBFT_FLAGS = -lft

# Flags, libraries, include
# Several compile modes are defined: no flags, debug, standard
ifdef MODE
ifeq ($(MODE), no)
endif
ifeq ($(MODE), debug)
	CFLAGS = -g
endif
ifeq ($(MODE), bug_address)
	CFLAGS = -g3 -fsanitize=address
endif
else
	CFLAGS = -Wall -Wextra -Werror
endif

all: libft $(NAME)
test: tests test_run

# compiling run binary
$(NAME): $(OBJS_MD5) $(OBJ_MD5_DIR)/$(OBJ_MAIN)
	@echo "$(Y)[COMPILING FT_SSL FILES]$(E)"
	@echo "$(G)$(CC) $(CFLAGS) $^ $(LIBFT_FLAGS) -L$(LIBFT_DIR) -o $@$(E)"
	@$(CC) $(CFLAGS) $^ $(LIBFT_FLAGS) -L$(LIBFT_DIR) -o $@
	@echo "$(Y)[COMPILLING DONE]$(E)"

# compiling binaries
./$(OBJ_MD5_DIR)/$(OBJ_MAIN): $(MAIN)
	@mkdir ./$(OBJ_MD5_DIR) 2> /dev/null || true
	@$(CC) $(CFLAGS) -I $(MD5_H) -I ./$(GLOBAL_H) -c $< -o $@

./$(OBJ_MD5_DIR)/%.o: ./$(SRC_MD5_DIR)/%.c
	@mkdir ./$(OBJ_MD5_DIR) 2> /dev/null || true
	@$(CC) $(CFLAGS) -I $(MD5_H) -I ./$(GLOBAL_H) -c $< -o $@

#########   Tests   #########
# Link library
tests: $(OBJS_TEST) $(OBJS_MD5)
	@echo "$(Y)[TEST COMPILATION]$(E)"
	@echo "$(G)$(CC) $(CFLAGS) $^ $(LIBFT_FLAGS) -L$(LIBFT_DIR) -o $(TEST_NAME)$(E)"
	@$(CC) $(CFLAGS) $^ $(LIBFT_FLAGS) -L$(LIBFT_DIR) -o $(TEST_NAME)
	@echo "$(Y)[TEST COMPILLING DONE]$(E)"

# Tests compilation
./$(TEST_DIR)/%.o: ./$(TEST_DIR)/%.c
	@$(CC) $(CFLAGS) -I $(MD5_H) -I ./$(GLOBAL_H) -c $< -o $@

test_run: tests
	@echo "$(Y)[START RUNNING TESTS]$(E)"
	@echo "---------------------"
	@./run_test
	@echo "---------------------"
	@echo "$(Y)[END TESTS]$(E)"

clean_test:
	@echo "$(Y)[CLEANING TEST FILES]$(E)"
	@echo "$(R)$(RM) $(OBJS_TEST)$(E)"
	@$(RM) ./$(OBJS_TEST) 2> /dev/null || true
	@echo "$(R)$(RM) $(TEST_NAME)$(E)"
	@$(RM) $(TEST_NAME) 2> /dev/null || true
	@echo "$(Y)[TEST CLEANING DONE]$(E)"

#############################

clean:
	@echo "$(Y)[CLEANING FT_SSL FILES]$(E)"
	@$(MAKE) -C $(LIBFT_DIR) clean
	@$(RM) -rf $(OBJ_MD5_DIR) 2> /dev/null || true
	@echo "$(Y)[CLEANING DONE]$(E)"

fclean: clean
	@echo "$(R)[$(NAME) DELETED]$(E)"
	@$(RM) -rf $(NAME)
	@$(MAKE) -C $(LIBFT_DIR) onlyfclean

libft:
	@echo "$(Y)[COMPILING LIBFT]$(E)"
	@echo "$(G)$(MAKE) -C $(LIBFT_DIR) MODE=$(MODE)$(E)"
	@$(MAKE) -C $(LIBFT_DIR) MODE=$(MODE)
	@echo "$(Y)[COMPILATION DONE]$(E)"

re: fclean all

# Non-file targets
.PHONY: all, re, clean, fclean, tests, test_run
