/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 11:43:01 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/10 18:10:59 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_tests.h"

int tests_run = 0;

//static char		*empty_str(void)
//{
//	mu_assert("error, test: empty_str failed", ft_strncmp(ft_md5(""), "d41d8cd98f00b204e9800998ecf8427e", 32) == 0);
//	return (0);
//}
//
//static char		*hello_str(void)
//{
//	mu_assert("error, test: hello_str failed", ft_strncmp(ft_md5("hello"), "5d41402abc4b2a76b9719d911017c592", 32) == 0);
//	return (0);
//}
//
//static char		*long_str(void)
//{
//	mu_assert("error, test: long_str failed", ft_strncmp(ft_md5("5d41402abc4b2a76b9719d911017c5925d41402abc4b2a76b9719d911017c5925d41402abc4b2a76b9719d911017c5925d41402abc4b2a76b9719d911017c592"), "b12bc24f5507eba4ee27092f70148415", 32) == 0);
//	return (0);
//}

//static char		*all_tests(void)
//{
//	mu_run_test(empty_str);
//	mu_run_test(hello_str);
//	mu_run_test(long_str);
//	return (0);
//}

static char		*test(char *message, int test)
{
	mu_assert(message, test);
	return (0);
}

int				main(void)
{
	char 		*result;
	char		*tmp;
	int			i;

	i = -1;
	//result = all_tests();
	while (input_test[++i].input)
	{
		tmp = ft_strjoin("Error, test for string: \"", input_test[i].input);
		tmp = ft_strjoin(tmp, "\", FAILED");

		result = test(tmp, ft_strncmp(ft_md5(input_test[i].input), input_test[i].input, 32) == 0);
	}

	if (result != 0)
		ft_putendl(result);
	else
		ft_putendl("ALL TESTS PASSED");
	ft_putstr("Tests run: ");
	ft_putnbrel(tests_run);
	return (0);
}
