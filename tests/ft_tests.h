/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tests.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 11:40:45 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/10 17:59:36 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_TESTS

# define DEBUG 0

# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# include "../md5/inc/md5.h"

#define mu_assert(message, test) do { if (!(test)) return message; } while (0)
#define mu_run_test(test) do { char *message = test(); tests_run++; \
		                                 if (message) return message; } while (0)
extern int		tests_run;

typedef struct	s_test
{
	char		*input;
}				t_test;

static const t_test	input_test[6] = {
	{ "" }, { "hello" }, { "1234567890-!@#$%^&*()_+" }, { "71I0n33qAxzlRtqhIliVRXvHNfo6MpEcSNBvtiD2bpwCKdG3AlQJy9wGmwslYf27" }, { "oS9bPHTwFLCZKNkMiPs4IPlPxcIZRTylV399ysZf7eGAtBulEUHb8oS63iglCeCV5l1AkNEsB2WGk2ivmsPXFD7Q035Jbbd481Nmi9kQUesCLXFYMHYLKhSAgFGzewPyztcD3k7vTsk10vJ3LqyOfzahGjerew6R5Sw7sLmSnkTtgp8I3oD6du2BUDPvDvnibqucUQmXlfu4fom08gv1cTylpJRHKQndiLTI3XPWwu7Jh5Cn8TFhuRPOLtgeVIdPmjeY0lgp2MEoPKJ5xFnlVwakmtp7xH2te9i8ur4LH5iLLyFesbVa3ZcrYCFXKPv5EyPMSJfiZzYkxgvi1aeRT3ltQyXbbLa3W6QUdknNfCX8NaPzV6N2rbpJWh7DZ75TMNyOkhmqeOUPGGh4v5VdIZtGlsZAu5svmGwdvQTR3gzGsaC5kLo8Ne3voPCUivf6ufbj5gdOOrXRxwHrTU4iQScPAAK2ZxHJn8AK24Y1XgbTkf6cD4aLKj0BLF5BCFUg6EDUlBeSqS3p92chrjD2CUYUkICL0CCpXT0RvPBfjQq804JMsoPBramVAen1m6oxTWvGHLztiDGNA14YFufaGJ4K9jVjoJx7StUqlFxkdhV0OkTgojhJmdaSDaOnma6j4XhzRqM5UOF3EbE9Pr99SlA2hxxatmVl6c2c4hidW8Wtmy02ZMAbAH9eTIbLZe56Vs86nzlJxvu1vQShHXLUgLzYAXaXrP3JJwC17oyJ9CXSqv18wFZ1L82T2itxSv1EUWhRc8Li5qQCfcVDLF2RHPKkNT0FdA0Zd2PYnlJJVpK40JmHF7DggcmPMoFEIqtoMfgOzxqfZbyMX7ZAghodWjbrIC6G7XKtYsU1C0R7c9mCzIQz6fgWZvWsK74RworgdeseltagMHe1yoKf20m1FvFysVxkfOpmjLZFKCAKA9hYLAWXu909zGgh7ZoKdLJW5bigrCNJvjimInYu0V51OVjnUcMwz2XXnIjV4JO7" }, { NULL }
};

#endif
