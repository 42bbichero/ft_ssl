/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 11:43:01 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/10 18:47:45 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5/inc/md5.h"

/*
** Determine array index from user input hash name
*/

static int		ft_hash_index(char *hash_name)
{
	int			index;

	index = -1;
	while (!fct_hash[++index].hash_name)
	{
		if (ft_strncmp(fct_hash[index].hash_name, hash_name, \
												ft_strlen(hash_name) == 0))
			return (index);
	}
	return (index);
}

/*
** Main function, will call function hash corresponding to user input
*/

int				main(int ac, char **av)
{
	int			i;
	char		*result;

	i = 0;
	if (ac != 2)
	{
		ft_putendl("Need arguments.");
		return (0);
	}
	result = fct_hash[ft_hash_index(av[1])].fun(av[1]);
	ft_putendl(result);
	free(result);
	return (0);
}

